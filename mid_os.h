#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <functional>

namespace os {

using time_t = long;

time_t time_ns(void);
time_t time_us(void);
static inline time_t time_ms(void) { return time_us() / 1E+3; }
static inline time_t time_s(void) { return time_us() / 1E+6; }

void delay_us(uint32_t us);
void delay_ms(uint32_t ms);

/* @ref https://www.jianshu.com/p/aa96876ebabc */
class timer_t {
  public:
  void *arg;
  using callback_t = std::function<void(timer_t *)>;
  timer_t(callback_t cb, void *arg = nullptr, const char name[] = nullptr);
  ~timer_t(void);
  void start(uint32_t period, bool reload = false);
  void stop(void);
  bool is_started(void);
  private:
  void *_handle;
  callback_t _cb;
};

/* @ref https://www.jianshu.com/p/88fdd500cf44 */
class task_t {
  public:
  void *arg;
  using callback_t = std::function<void(task_t *)>;
  task_t(callback_t cb, void *arg = nullptr, uint32_t stack_size = 1024, uint32_t priority = 15, const char name[] = nullptr);
  ~task_t(void);
  void exit(void *val = nullptr);
  void suspend(void);
  void resume(void);
  void notify(uint32_t events = 0);
  std::tuple<int, uint32_t> wait_notify(uint32_t timeout_ms = -1);
  private:
  void *_handle;
  callback_t _cb;
};

class mutex_t {
  public:
  mutex_t(void);
  ~mutex_t(void);
  void lock(uint32_t timeout_ms = -1);
  void unlock(void);
  private:
  void *_handle;
};

class lock_guard {
  public:
  lock_guard(mutex_t &mutex) : _mutex(mutex) { _mutex.lock(); }
  ~lock_guard() { _mutex.unlock(); }
  private:
  mutex_t &_mutex;
};

class event_t {
  public:
  event_t();
  ~event_t();
  void set(uint32_t bits);
  void clear(uint32_t bits);
  // waiting for all and clearing if set
  bool wait(uint32_t flags, uint32_t timeout_ms = -1);
  // waiting for any bit, not clearing them
  bool wait_any(uint32_t flags, uint32_t timeout_ms = -1);
  bool is_any(uint32_t flags);
  private:
  void *_handle;
};

class queue_t {
  public:
  queue_t(void);
  queue_t(int queue_length, int item_size);
  int recv(void *buff, uint32_t timeout_ms = -1);
  int send(void *buff, uint32_t timeout_ms = -1);
  int reset();
  int size();
  private:
  void *_handle;
};

bool in_isr_context();
void start_scheduler();

} /* namespace os */
