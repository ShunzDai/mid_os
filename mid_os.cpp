#include "mid_os.h"
#if defined __linux__ || defined __APPLE__
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <pthread.h>
#include <sys/time.h>
#elif defined ESP_PLATFORM
#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"
#include <sys/time.h>
#include <unistd.h>
#elif defined __arm__
#include "main.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "task.h"
#else /* platform */
#error "unsupported platform"
#endif /* platform */
#include <string.h>
#include <stdio.h>
#include <assert.h>

namespace os {

time_t time_ns(void) {
#if defined __linux__ || defined __APPLE__ || defined ESP_PLATFORM
  struct timespec timestamp;
  clock_gettime(CLOCK_REALTIME, &timestamp);
  return timestamp.tv_nsec;
#else /* platform */
  return 0;
#endif /* platform */
}

time_t time_us(void) {
#if defined __linux__ || defined __APPLE__ || defined ESP_PLATFORM
  timeval tv;
  gettimeofday(&tv, nullptr);
  return (time_t)(tv.tv_sec * 1000000 + tv.tv_usec);
#elif defined __arm__
  return 0;
#endif /* platform */
}

void delay_us(uint32_t us) {
#if defined __linux__ || defined __APPLE__ || defined ESP_PLATFORM
  usleep(us);
#else /* platform */

#endif /* platform */
}

void delay_ms(uint32_t ms) {
#if defined __linux__ || defined __APPLE__
  usleep(ms * 1000);
#elif defined ESP_PLATFORM || __arm__
  vTaskDelay(ms / portTICK_PERIOD_MS);
#else /* platform */

#endif /* platform */
}

timer_t::timer_t(callback_t cb, void *arg, const char name[])
#if defined __linux__
: arg(arg), _cb(cb) {
  typedef void (*_function) (__sigval_t);
  sigevent sev;
  memset(&sev, 0, sizeof(sev));
  sev.sigev_value.sival_ptr = this;
  sev.sigev_notify = SIGEV_THREAD;
  sev.sigev_notify_function = +[](__sigval_t val) {
    auto timer = *(timer_t **)&val;
    timer->_cb(timer);
  };
  sev.sigev_notify_attributes = nullptr;
  timer_create(CLOCK_MONOTONIC, &sev, (::timer_t *)&_handle);
}
#elif defined ESP_PLATFORM || __arm__
: arg(arg),
_handle((void *)xTimerCreate(name, 1000, pdFALSE, this, (TimerCallbackFunction_t)+[](TimerHandle_t handle) {
  auto timer = (timer_t *)(pvTimerGetTimerID(handle));
  timer->_cb(timer);
})),
_cb(cb) {
  assert(_handle);
}
#else /* platform */
: arg(arg), _cb(cb) {
  (void)_handle;
}
#endif /* platform */

timer_t::~timer_t(void) {
#if defined __linux__
  timer_delete((::timer_t)_handle);
#elif defined ESP_PLATFORM || __arm__
  xTimerDelete((TimerHandle_t)_handle, portMAX_DELAY);
#else /* platform */

#endif /* platform */
}

void timer_t::start(uint32_t period, bool reload) {
#if defined __linux__
  itimerspec ts;
  memset(&ts, 0, sizeof(ts));
  ts.it_value.tv_sec = period / 1000;
  ts.it_value.tv_nsec = (period % 1000) * 1000 * 1000;
  ts.it_interval.tv_sec = reload ? ts.it_value.tv_sec : 0;
  ts.it_interval.tv_nsec = reload ? ts.it_value.tv_nsec : 0;
  timer_settime((::timer_t)_handle, 0, &ts, nullptr);
#elif defined ESP_PLATFORM || __arm__
  vTimerSetReloadMode((TimerHandle_t)_handle, reload);
  xTimerChangePeriod((TimerHandle_t)_handle, pdMS_TO_TICKS(period), portMAX_DELAY);
#else /* platform */

#endif /* platform */
}

void timer_t::stop(void) {
#if defined __linux__
  itimerspec ts;
  memset(&ts, 0, sizeof(ts));
  timer_settime((::timer_t)_handle, 0, &ts, nullptr);
#elif defined ESP_PLATFORM || __arm__
  xTimerStop((TimerHandle_t)_handle, portMAX_DELAY);
#else /* platform */

#endif /* platform */
}

bool timer_t::is_started(void) {
#if defined __linux__
  itimerspec ts;
  memset(&ts, 0, sizeof(ts));
  timer_gettime((::timer_t)_handle, &ts);
  return ts.it_value.tv_sec != 0 || ts.it_value.tv_nsec != 0;
#elif defined ESP_PLATFORM || __arm__
  return xTimerIsTimerActive((TimerHandle_t)_handle);
#else /* platform */
  return false;
#endif /* platform */
}

task_t::task_t(callback_t cb, void* arg, uint32_t stack_size, uint32_t priority, const char name[])
#if defined __linux__ || defined __APPLE__
 : arg(arg), _cb(cb) {
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
  pthread_create((pthread_t *)&_handle, &attr, +[](void *handle) -> void * {
    auto task = (task_t *)(handle);
    task->_cb(task);
    task->exit();
    return nullptr;
  }, this);
}
#elif defined ESP_PLATFORM || __arm__
 : arg(arg), _cb(cb) {
  xTaskCreate(+[](void *handle) {
    auto task = (task_t *)(handle);
    task->_cb(task);
    task->exit();
  }, name, stack_size, this, priority, (TaskHandle_t *)&_handle);
  assert(_handle);
}
#else /* platform */
: arg(arg), _cb(cb) {

}
#endif /* platform */

task_t::~task_t(void) {
#if defined __linux__ || defined __APPLE__
  pthread_join((pthread_t)_handle, nullptr);
#elif defined ESP_PLATFORM || __arm__
  vTaskDelete((TaskHandle_t)_handle);
#else /* platform */

#endif /* platform */
}

void task_t::exit(void *val) {
#if defined __linux__ || defined __APPLE__
  pthread_exit(nullptr);
#elif defined ESP_PLATFORM
  vTaskDelete((TaskHandle_t)val);
#else /* platform */

#endif /* platform */
}

void task_t::suspend(void) {
#if defined __linux__ || defined __APPLE__
#elif defined ESP_PLATFORM
  vTaskSuspend((TaskHandle_t)_handle);
#else /* platform */

#endif /* platform */
}

void task_t::resume(void) {
#if defined __linux__ || defined __APPLE__
#elif defined ESP_PLATFORM
  vTaskResume((TaskHandle_t)_handle);
#else /* platform */

#endif /* platform */
}

void task_t::notify(uint32_t events) {
#if defined __linux__ || defined __APPLE__
#elif defined ESP_PLATFORM
  xTaskNotify((TaskHandle_t)_handle, events, eSetBits);
#else /* platform */

#endif /* platform */
}

std::tuple<int, uint32_t> task_t::wait_notify(uint32_t timeout_ms) {
#if defined __linux__ || defined __APPLE__
  return {0, 0};
#elif defined ESP_PLATFORM
  uint32_t events = 0;
  return {xTaskNotifyWait(0, 0xFFFFFFFF, &events, timeout_ms) ? 0 : -1, events};
#else /* platform */

#endif /* platform */
}

mutex_t::mutex_t(void)
#if defined __linux__ || defined __APPLE__
{
  pthread_mutex_init((pthread_mutex_t *)&_handle, nullptr);
  assert(_handle);
}
#elif defined ESP_PLATFORM
: _handle((void *)xSemaphoreCreateMutex()) {
  assert(_handle);
}
#else /* platform */
{

}
#endif /* platform */

mutex_t::~mutex_t(void) {
#if defined __linux__ || defined __APPLE__
#elif defined ESP_PLATFORM
  vSemaphoreDelete((SemaphoreHandle_t)_handle);
#else /* platform */

#endif /* platform */
}

void mutex_t::lock(uint32_t timeout_ms) {
#if defined __linux__ || defined __APPLE__
#elif defined ESP_PLATFORM
  xSemaphoreTake((SemaphoreHandle_t)_handle, timeout_ms);
#else /* platform */

#endif /* platform */
}

void mutex_t::unlock(void) {
#if defined __linux__ || defined __APPLE__
#elif defined ESP_PLATFORM
  xSemaphoreGive((SemaphoreHandle_t)_handle);
#else /* platform */

#endif /* platform */
}

event_t::event_t()
: _handle(xEventGroupCreate()) {
}

event_t::~event_t() {
  vEventGroupDelete((EventGroupHandle_t)_handle);
}

void event_t::set(uint32_t bits) {
  xEventGroupSetBits((EventGroupHandle_t)_handle, bits);
}

void event_t::clear(uint32_t bits) {
  xEventGroupClearBits((EventGroupHandle_t)_handle, bits);
}

bool event_t::wait(uint32_t flags, uint32_t timeout_ms) {
  EventBits_t bits = xEventGroupWaitBits((EventGroupHandle_t)_handle, flags, pdTRUE, pdTRUE, pdMS_TO_TICKS(timeout_ms));
  return bits & flags;
}

bool event_t::wait_any(uint32_t flags, uint32_t timeout_ms) {
  EventBits_t bits = xEventGroupWaitBits((EventGroupHandle_t)_handle, flags, pdFALSE, pdFALSE, pdMS_TO_TICKS(timeout_ms));
  return bits & flags;
}

bool event_t::is_any(uint32_t flags) {
  return xEventGroupGetBits((EventGroupHandle_t)_handle) & flags;
}

queue_t::queue_t(void) {

}

queue_t::queue_t(int queue_length, int item_size)
#if defined __linux__ || defined __APPLE__
{}
#elif defined ESP_PLATFORM
: _handle((void *)xQueueCreate(queue_length, item_size)) {
  assert(_handle);
}
#else /* platform */
{}
#endif /* platform */

int queue_t::recv(void *buff, uint32_t timeout_ms) {
  return xQueueReceive((QueueHandle_t)_handle, buff, pdMS_TO_TICKS(timeout_ms)) ? 0 : -1;
}

int queue_t::send(void *buff, uint32_t timeout_ms) {
  if(in_isr_context())
		return 0;
	else
		return xQueueSend((QueueHandle_t)_handle, buff, timeout_ms) ? 0 : -1;
}

int queue_t::reset() {
  return xQueueReset((QueueHandle_t)_handle);
}

int queue_t::size() {
  return uxQueueMessagesWaiting((QueueHandle_t)_handle);
}

bool in_isr_context(void) {
#if defined __linux__ || defined __APPLE__
  return false;
#elif defined ESP_PLATFORM
  return xPortInIsrContext();
#elif defined __arm__
  uint32_t result;
  __asm__ volatile ("MRS %0, ipsr" : "=r" (result) );
  return(result);
#endif
}

void start_scheduler(void) {
#if defined __linux__ || defined __APPLE__
#elif defined ESP_PLATFORM  || __arm__
  vTaskStartScheduler();
#else /* platform */

#endif /* platform */
}

} /* namespace os */
